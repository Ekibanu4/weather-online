package weather.crazyinc.com.weatheronline.models;

import java.util.Date;

/**
 * Created by OLEG on 09.08.2016.
 */
public class ForecastItem {
    private long date;
    private String icon;
    private Double temp;

    public long getDate() {
        return date;
    }

    public void setDate(long date) {
        this.date = date;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public Double getTemp() {
        return temp;
    }

    public void setTemp(Double temp) {
        this.temp = temp;
    }
}
