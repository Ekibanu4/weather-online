package weather.crazyinc.com.weatheronline.adapters;

import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Typeface;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.TextView;

import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import weather.crazyinc.com.weatheronline.R;
import weather.crazyinc.com.weatheronline.activities.StarterActivity;
import weather.crazyinc.com.weatheronline.dao.CityDao;
import weather.crazyinc.com.weatheronline.models.WeatherObject;

/**
 * Created by OLEG on 28.07.2016.
 */
public class CityAdapter extends ArrayAdapter<WeatherObject> {
    Context context;
    int resource;
    List<WeatherObject> cityWeathers;
    Typeface weathericons;

    public CityAdapter(Context context, int resource, List<WeatherObject> cityWeathers, Typeface weathericons) {
        super(context, resource, cityWeathers);
        this.context = context;
        this.resource = resource;
        this.cityWeathers = cityWeathers;
        this.weathericons = weathericons;


    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final CitiesHolder citiesHolder;
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(resource, parent, false);

            citiesHolder = new CitiesHolder(convertView);

            convertView.setTag(citiesHolder);
        } else {
            citiesHolder = (CitiesHolder) convertView.getTag();
        }
        final WeatherObject wObject = cityWeathers.get(position);


        Date dNow = new Date();
        int offset = 0;
        for (int it = 0; it < 40; it++) {
            long wd = wObject.getList().get(it).getDt() * 1000;
            Date weatherDate = new Date(wd);
            long diff = weatherDate.getTime() - dNow.getTime();
            if (diff > 0) {
                offset = it;
                break;
            }
        }

        citiesHolder.tvListCityName.setText(wObject.getCity().getName().toUpperCase()+", "+wObject.getCity().getCountry().toUpperCase());
        double temp = wObject.getList().get(offset).getMain().getTemp();
        temp = temp - 273.15;
        String t = String.format("%.0f", temp);
        t = t + context.getString(R.string.icon_celsius);
        String icon = "" + wObject.getList().get(offset).getWeather().get(0).getIcon();
        icon = "icon_" + icon;
        final int iconStringID = context.getApplicationContext().getResources().getIdentifier(icon, "string", context.getApplicationContext().getPackageName());

        citiesHolder.tvListTempereture.setText(t);
        citiesHolder.tvListTempereture.setTypeface(weathericons);
        citiesHolder.tvListWeatherStatus.setText(wObject.getList().get(offset).getWeather().get(0).getDescription());
        citiesHolder.tvListWeatherIcon.setTypeface(weathericons);
        citiesHolder.tvListWeatherIcon.setText(iconStringID);
        citiesHolder.buttonDeleteCity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder builder = new AlertDialog.Builder(context);
                builder.setTitle(R.string.acceptDelquestion);

                builder.setPositiveButton(R.string.dlgYesMesssage, new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int which) {

                        CityDao cd = new CityDao(context.getApplicationContext());
                        int id = cd.checkIfExist(wObject.getCity().getName().toUpperCase() + ", " + wObject.getCity().getCountry().toUpperCase());
                        cd.delCityByID(id);
                        ((StarterActivity)context).restartActivity();
                    }});
                builder.setNegativeButton(R.string.dlgNoMessage, new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        // Do nothing
                        dialog.dismiss();
                    }
                });
                builder.show();
                }
            });


        return convertView;
    }

    static class CitiesHolder {
        @BindView(R.id.tvListCityName)
        TextView tvListCityName;
        @BindView(R.id.tvListTempereture)
        TextView tvListTempereture;
        @BindView(R.id.tvListWeatherStatus)
        TextView tvListWeatherStatus;
        @BindView(R.id.tvListWeatherIcon)
        TextView tvListWeatherIcon;
        @BindView(R.id.buttonDelete)
        Button buttonDeleteCity;


        public CitiesHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }
}
