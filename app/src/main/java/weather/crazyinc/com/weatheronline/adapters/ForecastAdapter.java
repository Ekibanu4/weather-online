package weather.crazyinc.com.weatheronline.adapters;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.TextView;

import java.io.LineNumberInputStream;
import java.lang.reflect.Type;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import weather.crazyinc.com.weatheronline.R;
import weather.crazyinc.com.weatheronline.models.DataList;
import weather.crazyinc.com.weatheronline.models.ForecastItem;
import weather.crazyinc.com.weatheronline.models.WeatherObject;

/**
 * Created by OLEG on 08.08.2016.
 */

public class ForecastAdapter extends RecyclerView.Adapter<ForecastAdapter.ForecastHolder>  {
   private List<ForecastItem> data;

    public ForecastAdapter(Context context, int resource, List<ForecastItem> data,Typeface weathericons) {
        this.data = data;
        this.weathericons = weathericons;
        this.context = context;
        this.resource = resource;
    }
    Typeface weathericons;
    Context context;
    int resource;

    @Override
    public ForecastAdapter.ForecastHolder onCreateViewHolder(ViewGroup parent,
                                                   int viewType) {

        View itemLayoutView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.forecastitem, null);

        ForecastHolder forecastHolder = new ForecastHolder(itemLayoutView);
        return forecastHolder;
    }

    @Override
    public void onBindViewHolder(ForecastHolder forecastHolder, int position) {

        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");
        forecastHolder.tvForecastTime.setText(sdf.format(new Date(data.get(position).getDate() * 1000)));

        String icon = "" + data.get(position).getIcon();
        icon = "icon_" + icon;
        final int iconStringID = context.getApplicationContext().getResources().getIdentifier(icon, "string", context.getApplicationContext().getPackageName());
        forecastHolder.tvForecastIcon.setText(iconStringID);
        forecastHolder.tvForecastIcon.setTypeface(weathericons);
        double temp = data.get(position).getTemp();
        temp = temp - 273.15;
        String t = String.format("%.0f", temp)+" C";
        forecastHolder.tvForecastTemp.setText(t);
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    static class ForecastHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.tvForecastTime)
        TextView tvForecastTime;
        @BindView(R.id.tvForecastIcon)
        TextView tvForecastIcon;
        @BindView(R.id.tvForecastTemp)
        TextView tvForecastTemp;

        public ForecastHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }

}
