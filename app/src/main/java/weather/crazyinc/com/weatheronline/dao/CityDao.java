package weather.crazyinc.com.weatheronline.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;
import java.util.List;

import weather.crazyinc.com.weatheronline.models.Cities;

/**
 * Created by OLEG on 28.07.2016.
 */
public class CityDao {
    DbHelper dbHelper;
    Context context;
    SQLiteDatabase db;

    public CityDao(Context context) {
        this.context = context;
        this.dbHelper = new DbHelper(context);
    }

    public void saveCity(String cityname) {
        ContentValues cv = new ContentValues();
        db = dbHelper.getWritableDatabase();
        cv.put("cityname", cityname);
        db.insert("usercities", null, cv);
        db.close();
    }

    public List<Cities> getCityNames() {
        db = dbHelper.getWritableDatabase();
        final String[] projection = {"ID", "cityname"};
        Cursor c = db.query("usercities", projection, null, null, null, null, null);
        List<Cities> cities = new ArrayList<>();
        while (c.moveToNext()) {
            Cities s = new Cities();
            s.setName(c.getString(c.getColumnIndex("cityname")));
            s.setID(c.getInt(c.getColumnIndex("ID")));
            cities.add(s);
        }
        db.close();
        return cities;

    }

    public int checkIfExist(String city) {
        int id = -1;
        db = dbHelper.getWritableDatabase();
        final String[] projection = {"ID"};
        final String selection = "cityname='"+city+"'";
        String[] args = new String[]{city};
        int size = 0;
        Cursor c = db.query("usercities", projection, selection,null, null, null, null);
        if (c.moveToNext()) {
            id = c.getInt(c.getColumnIndex("ID"));
        }
        db.close();
        return id;
    }

    public void delCityByID(int ID){
        db = dbHelper.getWritableDatabase();
        db.delete("usercities", "id = " + ID, null);
        db.close();
    };

    public void getIdByName(String cityName){
        db = dbHelper.getWritableDatabase();
        db.delete("usercities", "cityname = '" + cityName.toUpperCase()+"'", null);
        db.close();
            }


}
