package weather.crazyinc.com.weatheronline.activities;

import android.content.Intent;
import android.os.StrictMode;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.Toast;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnItemClick;
import retrofit2.Call;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.GET;
import retrofit2.http.QueryMap;
import weather.crazyinc.com.weatheronline.R;
import weather.crazyinc.com.weatheronline.dao.CityDao;
import weather.crazyinc.com.weatheronline.models.Geonames;

public class CitySearchActivity extends AppCompatActivity implements TextWatcher {
    @BindView(R.id.citieslistview)
    EditText autoCompleteTextView;
    @BindView(R.id.listViewSearch)
    ListView listViewSearch;
    @BindView(R.id.buttonADD)
    Button addButton;
    ListAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_city_search);
        ButterKnife.bind(this);
        autoCompleteTextView.addTextChangedListener(this);
    }

    @Override
    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @Override
    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        if (autoCompleteTextView.getText().length() > 2) {
            final String in = autoCompleteTextView.getText().toString().toLowerCase();
            final Geonames[] geonames = {new Geonames()};
//            String[] names = new String[0];
            ArrayList<String> names = new ArrayList<String>();

            if (android.os.Build.VERSION.SDK_INT > 9) {
                StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
                StrictMode.setThreadPolicy(policy);
            }
            new

                    Thread() {

                        public void run() {
                            Map<String, String> data = new HashMap<>();
                            data.put("q", in);
                            data.put("lang", Locale.getDefault().getLanguage());
                            data.put("maxRows", "15");
                            data.put("style", "LONG");
                            data.put("username", "crazyweather");
                            data.put("type", "json");
                            data.put("fuzzy", "0.6");
                            data.put(" orderby", "relevance");
                            try {
                                GeonamesGetter getGeonames = GeonamesGetter.retrofit.create(GeonamesGetter.class);
                                Call<Geonames> call = getGeonames.getGeonames(data);
                                Response<Geonames> gaonames = call.execute();
                                geonames[0] = gaonames.body();
                            } catch (IOException e) {
                                e.printStackTrace();

                            }

                        }
                    }.run();

            if (geonames[0].getGeonames().size() > 0) {
                for (int pos = 0; pos < geonames[0].getGeonames().size(); pos++) {
                    if (geonames[0].getGeonames().get(pos).getFcl().toUpperCase().equals("P")) {

                        names.add(geonames[0].getGeonames().get(pos).getToponymName().toString().toUpperCase() + ", " + geonames[0].getGeonames().get(pos).getCountryCode().toString().toUpperCase());

                    }
                }

                adapter = new ArrayAdapter<String>(this, R.layout.searchitem, R.id.cityname, names);
                listViewSearch.setAdapter(adapter);

            }

        } else {
            listViewSearch.setAdapter(null);
        }


    }

    @Override
    public void afterTextChanged(Editable editable) {

    }


    public interface GeonamesGetter {
        @GET("search")
        Call<Geonames> getGeonames(
                @QueryMap Map<String, String> options
        );

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("http://api.geonames.org/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();
    }

    @OnClick(R.id.buttonADD)
    public void submit() {
        String s = autoCompleteTextView.getText().toString().trim();
        if (s.length() == 0) {

            Toast.makeText(CitySearchActivity.this, R.string.emptyCityToast, Toast.LENGTH_LONG).show();
        } else {
            CityDao cd = new CityDao(CitySearchActivity.this);

            int id = cd.checkIfExist(autoCompleteTextView.getText().toString().toUpperCase());
            if (id != -1) {
                Toast.makeText(CitySearchActivity.this, "Citi is in the list alredy.", Toast.LENGTH_LONG).show();
            } else {

                Intent newIntent = new Intent();
                Bundle bundle = new Bundle();
                bundle.putString("cityname", s.toUpperCase());
                bundle.putString("TAG", "CITY_ADD");
                newIntent.putExtras(bundle);
                setResult(RESULT_OK, newIntent);
                finish();
            }
        }
    }

    @OnItemClick(R.id.listViewSearch)
    public void onItemClick(AdapterView<?> adapterView, View view, int pos, long l) {
        String selectedFromList = (listViewSearch.getItemAtPosition(pos).toString());
        autoCompleteTextView.setText(selectedFromList);
        listViewSearch.setAdapter(null);
    }
}
