package weather.crazyinc.com.weatheronline.activities;

import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;


import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import lecho.lib.hellocharts.model.Axis;
import lecho.lib.hellocharts.model.Line;
import lecho.lib.hellocharts.model.LineChartData;
import lecho.lib.hellocharts.model.PointValue;
import lecho.lib.hellocharts.model.ValueShape;
import lecho.lib.hellocharts.model.Viewport;
import lecho.lib.hellocharts.view.LineChartView;
import retrofit2.Call;
import retrofit2.Response;
import weather.crazyinc.com.weatheronline.R;
import weather.crazyinc.com.weatheronline.fragments.ForecastFragment;
import weather.crazyinc.com.weatheronline.helper.WeatherGetter;
import weather.crazyinc.com.weatheronline.dao.CityDao;

import weather.crazyinc.com.weatheronline.models.ForecastItem;
import weather.crazyinc.com.weatheronline.models.WeatherObject;

public class CityWeatherActivity extends AppCompatActivity {
    String city;
    FragmentManager fragmentManager;
    @BindView(R.id.tvCityNameParameter)
    TextView tvCityNameParameter;
    @BindView(R.id.tvICONDate1)
    TextView tvICONDate1;
    @BindView(R.id.tvDate4)
    TextView tvDate4;
    @BindView(R.id.tvDate3)
    TextView tvDate3;
    @BindView(R.id.tvDate1)
    TextView tvDate1;
    @BindView(R.id.tvDate2)
    TextView tvDate2;
    @BindView(R.id.tvICONDate2)
    TextView tvICONDate2;
    @BindView(R.id.tvICONDate3)
    TextView tvICONDate3;
    @BindView(R.id.tvICONDate4)
    TextView tvICONDate4;
    @BindView(R.id.tvICON)
    TextView tvICON;
    @BindView(R.id.tvMainParameter)
    TextView tvMainParameter;
    @BindView(R.id.tvUpdatedAt)
    TextView tvUpdatedAt;
    @BindView(R.id.tvPressureIcon)
    TextView tvPressureIcon;
    @BindView(R.id.tvPressureParameter)
    TextView tvPressureParameter;
    @BindView(R.id.tvHumudityIcon)
    TextView tvHumudityicon;
    @BindView(R.id.tvHumudityParameter)
    TextView tvHumudityParameter;
    @BindView(R.id.tvWindIcon)
    TextView tvWindIcon;
    @BindView(R.id.tvWindParameter)
    TextView tvWindParameter;
    @BindView(R.id.tvTemperatureParameter)
    TextView tvTemperatureParameter;
    @BindView(R.id.lineChartViewDay)
    LineChartView lineChartViewDay;
    @BindView(R.id.lineChartViewNight)
    LineChartView lineChartViewNight;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_weather);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        ButterKnife.bind(this);

       if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (Build.VERSION.SDK_INT < 23 || checkSelfPermission("android.permission.INTERNET") == PackageManager.PERMISSION_GRANTED) {

            } else {
                requestPermissions(new String[]{Manifest.permission.INTERNET}, 100);
            }
        }

        CityDao cityDao = new CityDao(this);
        int i = getIntent().getIntExtra("cityname", 0);
        city = cityDao.getCityNames().get(i).getName();
        updateWeatherData(city);

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case 100:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Toast.makeText(this, R.string.accessOK, Toast.LENGTH_SHORT)
                            .show();
                } else {
                    Toast.makeText(this, R.string.errorNoAccesss, Toast.LENGTH_SHORT)
                            .show();
                }
                break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_weather, menu);
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.deleteCity:
                String c = tvCityNameParameter.getText().toString().toUpperCase();
                showDeleteDialog(c);
                break;
        }

        return super.onOptionsItemSelected(item);
    }


    private void updateWeatherData(final String city) {
        new Thread() {

            public void run() {

                Map<String, String> data = new HashMap<>();
                data.put("q", city);
                data.put("APPID", getString(R.string.APPIDKEY));
                data.put("lang", Locale.getDefault().getLanguage());
                try {
                    WeatherGetter.GetWeatherObject getWeatherObject = WeatherGetter.GetWeatherObject.retrofit.create(WeatherGetter.GetWeatherObject.class);
                    Call<WeatherObject> call = getWeatherObject.getWeatherObject(data);

                    Response<WeatherObject> weatherObject = call.execute();

                    final WeatherObject wObject = weatherObject.body();

                    if (wObject.getCod() == 200) {
                        CityWeatherActivity.this.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Toast.makeText(CityWeatherActivity.this, R.string.connectseccesfull, Toast.LENGTH_LONG).show();
                                renderWeather(wObject);
                            }

                        });
                    } else {
                        CityWeatherActivity.this.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Toast.makeText(CityWeatherActivity.this, R.string.errorNoAccesss, Toast.LENGTH_LONG).show();
                            }
                        });
                    }

                } catch (IOException e) {
                    e.printStackTrace();
                    CityWeatherActivity.this.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(CityWeatherActivity.this, R.string.errorNoAccesss, Toast.LENGTH_LONG).show();
                        }
                    });
                }
            }
        }.start();
    }


    private void renderWeather(WeatherObject weatherObject) {
        Typeface weathericons = Typeface.createFromAsset(CityWeatherActivity.this.getAssets(), "fonts/icons.ttf");
        int nextday = 0;

        try {
            Date dNow = new Date();
            int offset = 0;
            for (int it = 0; it < 40; it++) {
                long wd = weatherObject.getList().get(it).getDt() * 1000;
                Date weatherDate = new Date(wd);
                long diff = weatherDate.getTime() - dNow.getTime();
                if (diff > 0) {
                    offset = it;
                    break;
                }
            }


            ForecastFragment forecastFragment = new ForecastFragment();
            fragmentManager = getSupportFragmentManager();

            List<ForecastItem> data = new ArrayList();

            for (int i = 0; i < 8; i++) {
                ForecastItem fc = new ForecastItem();
                fc.setDate(weatherObject.getList().get(i).getDt());
                fc.setIcon(weatherObject.getList().get(i).getWeather().get(0).getIcon());
                fc.setTemp(weatherObject.getList().get(i).getMain().getTemp());
                data.add(fc);
            }
            forecastFragment.setData(data);
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.add(R.id.forecastContainer, forecastFragment, city);
            fragmentTransaction.commit();


            tvCityNameParameter.setText(weatherObject.getCity().getName().toUpperCase() +
                    ", " + weatherObject.getCity().getCountry());
            tvHumudityParameter.setText(weatherObject.getList().get(offset).getMain().getHumidity() + "%");
            tvPressureParameter.setText(String.format("%.2f", weatherObject.getList().get(offset).getMain().getPressure() / 1.3332239) + getString(R.string.pressureUnits));
            tvMainParameter.setText(weatherObject.getList().get(offset).getWeather().get(0).getDescription());
            double temp = weatherObject.getList().get(offset).getMain().getTemp();
            temp = temp - 273.15;
            String t = String.format("%.2f", temp);
            t = t + getString(R.string.icon_celsius);
            tvTemperatureParameter.setText(t);
            tvTemperatureParameter.setTypeface(weathericons);
            tvWindParameter.setText(weatherObject.getList().get(offset).getWind().getSpeed() + getString(R.string.detailMS));
            DateFormat df = DateFormat.getDateTimeInstance();
            String updatedOn = df.format(new Date(weatherObject.getList().get(offset).getDt() * 1000));
            tvUpdatedAt.setText(getString(R.string.lastUpdate) + updatedOn);
            String iconaMAnaMana = "" + weatherObject.getList().get(offset).getWeather().get(0).getIcon();
            iconaMAnaMana = "icon_" + iconaMAnaMana;
            final int iconStringID = CityWeatherActivity.this.getResources().getIdentifier(iconaMAnaMana, "string", CityWeatherActivity.this.getApplicationContext().getPackageName());
            tvICON.setText(iconStringID);
            tvICON.setTypeface(weathericons);
            tvPressureIcon.setTypeface(weathericons);
            tvHumudityicon.setTypeface(weathericons);
            tvWindIcon.setTypeface(weathericons);
            double degree = weatherObject.getList().get(offset).getWind().getDeg();
            windIconResolwer(degree);

            for (int it = 1; it < 40; it++) {
                String check = weatherObject.getList().get(it).getDt_txt();
                if (check.toLowerCase().contains("00:00:00".toLowerCase())) {
                    nextday = it;
                    break;
                }
            }


            List<PointValue> valuesday = new ArrayList<PointValue>();
            List<PointValue> valuesnight = new ArrayList<PointValue>();
            double tempDay = 0;
            double tempNight = 0;
            double minday = 9999;
            double minnight = 9999;
            double maxday = -9999;
            double maxnight = -9999;
            Date date;
            SimpleDateFormat ft = new SimpleDateFormat("dd/MM");
            String tempIcon = "";
            for (int it = 0; it < 4; it++) {

                date = new Date(weatherObject.getList().get(it * 8 + nextday).getDt() * 1000);
                tempIcon = "" + weatherObject.getList().get(it * 8 + nextday + 4).getWeather().get(0).getIcon();
                tempIcon = "icon_" + tempIcon;
                final int iconID = CityWeatherActivity.this.getResources().getIdentifier(tempIcon, "string", CityWeatherActivity.this.getApplicationContext().getPackageName());


                switch (it) {
                    case 0:
                        tvDate1.setText(ft.format(date));
                        tvICONDate1.setText(iconID);
                        tvICONDate1.setTypeface(weathericons);
                        break;
                    case 1:
                        tvDate2.setText(ft.format(date));
                        tvICONDate2.setText(iconID);
                        tvICONDate2.setTypeface(weathericons);
                        break;
                    case 2:
                        tvDate3.setText(ft.format(date));
                        tvICONDate3.setText(iconID);
                        tvICONDate3.setTypeface(weathericons);
                        break;
                    case 3:
                        tvDate4.setText(ft.format(date));
                        tvICONDate4.setText(iconID);
                        tvICONDate4.setTypeface(weathericons);
                        break;
                }

                tempDay = weatherObject.getList().get(it * 8 + 4 + nextday).getMain().getTemp();
                tempNight = weatherObject.getList().get((it * 8 + nextday)).getMain().getTemp();

                tempDay = tempDay - 273.15;
                tempNight = tempNight - 273.15;

                if (tempDay < minday) {
                    minday = tempDay;
                }
                if (tempDay > maxday) {
                    maxday = tempDay;
                }
                if (tempNight < minnight) {
                    minnight = tempNight;
                }
                if (tempNight > maxnight) {
                    maxnight = tempNight;
                }

                valuesday.add(new PointValue(it, (float) tempDay));
                valuesnight.add(new PointValue(it, (float) tempNight));

            }

            Line lineday = new Line(valuesday);
            Line linenight = new Line(valuesnight);

            lineday.setHasLabels(true);
            linenight.setHasLabels(true);

            lineday.setHasLines(true);
            linenight.setHasLines(true);

            linenight.setColor(Color.parseColor("#25486B"));
            lineday.setColor(Color.parseColor("#316AA3"));

            lineday.setShape(ValueShape.CIRCLE);
            linenight.setShape(ValueShape.CIRCLE);

            lineday.setCubic(true);
            linenight.setCubic(true);

            lineday.setFilled(false);
            linenight.setFilled(false);

            linenight.setPointColor(Color.parseColor("#292994"));
            lineday.setPointColor(Color.parseColor("#1D568F"));


            List<Line> linesDay = new ArrayList<Line>();
            List<Line> linesnight = new ArrayList<Line>();
            linesDay.add(lineday);
            linesnight.add(linenight);

            LineChartData dataDay = new LineChartData();
            LineChartData dataNight = new LineChartData();
//            Axis axisX = new Axis();
            Axis axisYDay = new Axis().setHasLines(true);
            Axis axisYNight = new Axis().setHasLines(true);
//            axisX.setName("Axis X");
            axisYDay.setName(getString(R.string.chartDay));
            axisYNight.setName(getString(R.string.chartNight));
//            axisX.setTextColor(Color.WHITE);
            axisYDay.setTextColor(Color.WHITE);
            axisYNight.setTextColor(Color.WHITE);
//            axisX.setTextSize(14);
            axisYDay.setTextSize(14);
            axisYNight.setTextSize(14);
//            data.setAxisXBottom(axisX);
            dataDay.setAxisYLeft(axisYDay);
            dataNight.setAxisYLeft(axisYNight);
//            dataDay.setBaseValue(0);
//            dataNight.setBaseValue(0);
            dataDay.setLines(linesDay);
            dataNight.setLines(linesnight);
            dataDay.setValueLabelsTextColor(Color.WHITE);
            dataNight.setValueLabelsTextColor(Color.WHITE);
            dataDay.setValueLabelTextSize(15);
            dataNight.setValueLabelTextSize(15);
            lineChartViewDay.setInteractive(false);
            lineChartViewNight.setInteractive(false);
            lineChartViewDay.setLineChartData(dataDay);
            lineChartViewNight.setLineChartData(dataNight);

            Viewport vD = new Viewport(lineChartViewDay.getMaximumViewport());
            vD.bottom = (float) (minday - 3);
            vD.top = (float) (maxday + 3);
            lineChartViewDay.setMaximumViewport(vD);
            lineChartViewDay.setCurrentViewportWithAnimation(vD);

            Viewport vN = new Viewport(lineChartViewNight.getMaximumViewport());
            vN.bottom = (float) (minnight - 3);
            vN.top = (float) (maxnight + 3);
            lineChartViewNight.setMaximumViewport(vN);
            lineChartViewNight.setCurrentViewportWithAnimation(vN);


        } catch (Exception e) {
            Toast.makeText(this, R.string.noConnectionToast, Toast.LENGTH_LONG).show();
        }
    }

    private void showDeleteDialog(final String cityname) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(R.string.dlgRUshure);

        builder.setPositiveButton(R.string.dlgYesMesssage, new DialogInterface.OnClickListener() {

            public void onClick(DialogInterface dialog, int which) {
                CityDao cd = new CityDao(getApplicationContext());
                int ID = cd.checkIfExist(cityname);
                if (ID < 0) {
                    return;
                } else {
                    cd.delCityByID(ID);
                    Intent intent = new Intent();
                    intent.putExtra("deleted", 1);
                    intent.putExtra("TAG", "CITY_WEATHER");
                    setResult(RESULT_OK, intent);
                    dialog.dismiss();
                    finish();
                }

            }
        });

        builder.setNegativeButton(R.string.dlgNoMessage, new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {

                // Do nothing
                dialog.dismiss();
            }
        });
        builder.show();

    }


    private void windIconResolwer(double degree) {
        degree = degree + 180;

        if (degree >= 360) {
            degree = degree - 360;
        }

        if (degree >= 337.5 || degree >= 0 && degree < 22.5) {
            tvWindIcon.setText(R.string.icon_wind_0);
        }
        if (degree >= 22.5 && degree < 67.5) {
            tvWindIcon.setText(R.string.icon_wind_45);
        }
        if (degree >= 67.5 && degree < 112.5) {
            tvWindIcon.setText(R.string.icon_wind_90);
        }
        if (degree >= 112.5 && degree < 157.5) {
            tvWindIcon.setText(R.string.icon_wind_135);
        }
        if (degree >= 157.5 && degree < 202.5) {
            tvWindIcon.setText(R.string.icon_wind_180);
        }
        if (degree >= 202.5 && degree < 247.5) {
            tvWindIcon.setText(R.string.icon_wind_225);
        }
        if (degree >= 247.5 && degree < 292.5) {
            tvWindIcon.setText(R.string.icon_wind_270);
        }
        if (degree >= 292.5 && degree < 337.5) {
            tvWindIcon.setText(R.string.icon_wind_315);
        }

    }


}

