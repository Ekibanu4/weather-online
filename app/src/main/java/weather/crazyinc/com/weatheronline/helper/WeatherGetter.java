package weather.crazyinc.com.weatheronline.helper;

import android.content.Context;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Map;

import org.json.JSONObject;

import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.GET;
import retrofit2.http.PATCH;
import retrofit2.http.Path;
import retrofit2.http.Query;
import retrofit2.http.QueryMap;
import weather.crazyinc.com.weatheronline.models.WeatherObject;

/**
 * Created by OLEG on 21.07.2016.
 */
public class WeatherGetter {

    public interface GetWeatherObject {
        @GET("data/2.5/forecast")
        Call<WeatherObject> getWeatherObject(
                @QueryMap Map<String, String> options
        );

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("http://api.openweathermap.org/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();
    }
}



