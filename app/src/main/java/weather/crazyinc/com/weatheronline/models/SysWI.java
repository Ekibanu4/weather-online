package weather.crazyinc.com.weatheronline.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by OLEG on 25.07.2016.
 */
public class SysWI {
    @SerializedName("pod")
    @Expose
   private String pod;

    public String getPod() {
        return pod;
    }

    public void setPod(String pod) {
        this.pod = pod;
    }
}
