package weather.crazyinc.com.weatheronline.fragments;

import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import weather.crazyinc.com.weatheronline.R;
import weather.crazyinc.com.weatheronline.adapters.ForecastAdapter;
import weather.crazyinc.com.weatheronline.models.DataList;
import weather.crazyinc.com.weatheronline.models.ForecastItem;

/**
 * Created by OLEG on 08.08.2016.
 */
public class ForecastFragment extends Fragment{
    List<ForecastItem> data;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        ButterKnife.bind(getActivity());

    View view = inflater.inflate(R.layout.forecast_fragment, null);


        LinearLayoutManager layoutManager
                = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
        RecyclerView forecastReRecyclerView = (RecyclerView) view.findViewById(R.id.forecastRecyclerView);
        forecastReRecyclerView.setLayoutManager(layoutManager);

        Typeface weathericons=Typeface.createFromAsset(getActivity().getAssets(),"fonts/icons.ttf");

        ForecastAdapter forecastAdapter = new ForecastAdapter(getActivity(), R.layout.forecastitem, data, weathericons);
        forecastReRecyclerView.setAdapter(forecastAdapter);
        forecastReRecyclerView.setItemAnimator(new DefaultItemAnimator());

        return view;
}

    public void setData(List<ForecastItem> data) {
        this.data = data;
    }
}
