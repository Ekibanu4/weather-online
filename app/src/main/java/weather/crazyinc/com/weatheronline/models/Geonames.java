package weather.crazyinc.com.weatheronline.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.*;
import java.util.List;

/**
 * Created by OLEG on 05.08.2016.
 */
public class Geonames {

    @SerializedName("geonames")
    @Expose
    private java.util.List<Geoname> geonames;
    @SerializedName("totalResultsCount")
    @Expose
    private String totalResultsCount;

    public List<Geoname> getGeonames() {
        return geonames;
    }

    public void setGeonames(List<Geoname> geonames) {
        this.geonames = geonames;
    }

    public String getTotalResultsCount() {
        return totalResultsCount;
    }

    public void setTotalResultsCount(String totalResultsCount) {
        this.totalResultsCount = totalResultsCount;
    }
}