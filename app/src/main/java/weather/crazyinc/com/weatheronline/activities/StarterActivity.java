package weather.crazyinc.com.weatheronline.activities;

import android.content.Intent;
import android.os.StrictMode;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Response;
import weather.crazyinc.com.weatheronline.R;
import weather.crazyinc.com.weatheronline.helper.WeatherGetter;
import weather.crazyinc.com.weatheronline.dao.CityDao;
import weather.crazyinc.com.weatheronline.fragments.CityChoseFragment;
import weather.crazyinc.com.weatheronline.models.Cities;
import weather.crazyinc.com.weatheronline.models.WeatherObject;

public class StarterActivity extends AppCompatActivity {
    final int REQUEST_CODE_ADD_CITY = 1;
    final int REQUEST_CODE_CITY_WEATHER = 65538;

    FragmentManager fragmentManager;
    CityChoseFragment cityChoseFragment;


    @BindView(R.id.buttonAddCity)
    Button buttonAddCity;
    @BindView(R.id.tvAddCity)
    TextView tvAddCity;
    @BindView(R.id.footerStarter)
    LinearLayout footerStarter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.starter);
        ButterKnife.bind(this);

        CityDao cityDao = new CityDao(this);
        List<Cities> cities = cityDao.getCityNames();
        if (!cities.isEmpty()) {
            buttonAddCity.setVisibility(View.GONE);
            tvAddCity.setVisibility(View.GONE);

        } else {
            footerStarter.setVisibility(View.GONE);
            buttonAddCity.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(StarterActivity.this, CitySearchActivity.class);
                    startActivityForResult(intent, REQUEST_CODE_ADD_CITY);
                }
            });
        }

        cityChoseFragment = new CityChoseFragment();
        fragmentManager = getSupportFragmentManager();

        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.add(R.id.container, cityChoseFragment, null);

        fragmentTransaction.commit();

    }

    public void restartActivity() {
        Intent intent = getIntent();
        finish();
        startActivity(intent);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                case REQUEST_CODE_ADD_CITY:
                    if (data.getStringExtra("TAG").equals("CITY_ADD")) {
                        CityAddResolver(data.getStringExtra("cityname"));
                    }
                    break;
                case REQUEST_CODE_CITY_WEATHER:
                    restartActivity();
                    break;
            }

        }

    }

    private void CityAddResolver(String cityname) {

        if (android.os.Build.VERSION.SDK_INT > 9) {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }
        final WeatherObject[] checkCity = {new WeatherObject()};
        final String in = cityname.toString();
        new Thread() {

            public void run() {
                Map<String, String> data = new HashMap<>();
                data.put("q", in);
                data.put("APPID", getString(R.string.APPIDKEY));
                data.put("lang", Locale.getDefault().getLanguage());
                try {
                    WeatherGetter.GetWeatherObject getWeatherObject = WeatherGetter.GetWeatherObject.retrofit.create(WeatherGetter.GetWeatherObject.class);
                    Call<WeatherObject> call = getWeatherObject.getWeatherObject(data);

                    Response<WeatherObject> weatherObject = call.execute();

                    final WeatherObject wO = weatherObject.body();
                    checkCity[0] = wO;

                } catch (IOException e) {
                    e.printStackTrace();

                }

            }
        }.run();
        if (checkCity[0].getCod() == 200) {
            CityDao cityDao = new CityDao(StarterActivity.this);
            cityDao.saveCity(checkCity[0].getCity().getName().toUpperCase() + ", " + checkCity[0].getCity().getCountry().toUpperCase());
            restartActivity();
        }
    }

    @OnClick({R.id.footerStarter,R.id.buttonAddCity})
    public void Click(View view) {
        Intent intent = new Intent(StarterActivity.this, CitySearchActivity.class);
        startActivityForResult(intent, REQUEST_CODE_ADD_CITY);
    }
}