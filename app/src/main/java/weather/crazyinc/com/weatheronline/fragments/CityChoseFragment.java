package weather.crazyinc.com.weatheronline.fragments;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import retrofit2.Call;
import retrofit2.Response;
import weather.crazyinc.com.weatheronline.activities.CityWeatherActivity;
import weather.crazyinc.com.weatheronline.R;
import weather.crazyinc.com.weatheronline.helper.WeatherGetter;
import weather.crazyinc.com.weatheronline.adapters.CityAdapter;
import weather.crazyinc.com.weatheronline.dao.CityDao;
import weather.crazyinc.com.weatheronline.models.Cities;
import weather.crazyinc.com.weatheronline.models.WeatherObject;

/**
 * Created by OLEG on 28.07.2016.
 */
public class CityChoseFragment extends Fragment {


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.cityfragment, null);
        CityDao cityDao = new CityDao(getActivity());
        List<Cities> cities = cityDao.getCityNames();
        ListView citiesListView = (ListView) view.findViewById(R.id.listViewCities);


        final List<WeatherObject> cityWeathers = new ArrayList<WeatherObject>();
        ExecutorService pool = Executors.newFixedThreadPool(1);
        for (int current = 0; current < cities.size(); current++) {

            final String citiname = cities.get(current).getName();

            try {
                pool.submit(

                        new Runnable() {
                            @Override
                            public void run() {
                                Map<String, String> data = new HashMap<>();
                                data.put("q", citiname);
                                data.put("APPID", getString(R.string.APPIDKEY));
                                data.put("lang", Locale.getDefault().getLanguage());
                                try {
                                    WeatherGetter.GetWeatherObject getWeatherObject = WeatherGetter.GetWeatherObject.retrofit.create(WeatherGetter.GetWeatherObject.class);
                                    Call<WeatherObject> call = getWeatherObject.getWeatherObject(data);

                                    Response<WeatherObject> weatherObject = call.execute();

                                    final WeatherObject wObject = weatherObject.body();

                                    if (wObject.getCod() == 200) {
                                        cityWeathers.add(wObject);

                                    } else {
                                    }

                                } catch (IOException e) {
                                    e.printStackTrace();

                                }

                            }
                        }
               ).get();
            } catch (InterruptedException | ExecutionException e) {
                e.printStackTrace();
            }
        }


        citiesListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Intent intent = new Intent(getActivity(), CityWeatherActivity.class);
                intent.putExtra("cityname", i);
                startActivityForResult(intent,2);
            }
        });

        Typeface weathericons=Typeface.createFromAsset(getActivity().getAssets(),"fonts/icons.ttf");
        CityAdapter cityAdapter = new CityAdapter(getActivity(), R.layout.citylistitem, cityWeathers, weathericons);
        citiesListView.setAdapter(cityAdapter);

        return view;
    }
}
