package weather.crazyinc.com.weatheronline.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by OLEG on 25.07.2016.
 */
public class Sys {
    @SerializedName("population")
    @Expose
    private int population;

    public int getPopulation() {
        return population;
    }

    public void setPopulation(int population) {
        this.population = population;
    }
}
