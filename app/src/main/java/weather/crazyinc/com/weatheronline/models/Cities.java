package weather.crazyinc.com.weatheronline.models;

/**
 * Created by OLEG on 28.07.2016.
 */
public class Cities {
    private String name;
    private int ID;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }
}
